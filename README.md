# README #

This is the Arduino source code for making the Warp Core LEDs do what they do. :)

### How do I get set up? ###

* Set up your circuit
* Copy and paste the code into your Arduino IDE
* Change code to match your circuit
* Run it!
* Wonder at the glowy goodness

### Who do I talk to? ###

* [da email:] Andy [:at:] StuffAndyMakes [:dot:] com
